/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appindexing.fetchasgoogle;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpExecuteInterceptor;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Lists;
import com.google.api.services.fetch_as_google_pa.FetchAsGooglePa;
import com.google.api.services.fetch_as_google_pa.model.Apk;
import com.google.api.services.fetch_as_google_pa.model.ApkDataRef;
import com.google.api.services.fetch_as_google_pa.model.ApkHolder;
import com.google.api.services.fetch_as_google_pa.model.FetchRequest;
import com.google.api.services.fetch_as_google_pa.model.FetchResponse;
import com.google.api.services.fetch_as_google_pa.model.Intent;
import com.google.api.services.fetch_as_google_pa.model.Operation;
import com.google.api.services.fetch_as_google_pa.model.ReferencedResource;
import com.google.gct.login.CredentialedUser;
import com.google.gct.login.GoogleLogin;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * The client which talk to "Fetch as Google" backend server.
 * This will allow a developer to submit their APK to Google so they can get a report about how
 * Google sees and indexes it.
 */
public final class FetchAsGoogleClient {
  private static final NetHttpTransport httpTransport = new NetHttpTransport();
  private static final JsonFactory jsonFactory = new JacksonFactory();
  private static final String SERVER_PATH = "";
  private static final String ROOT_URL = "https://test-fetch-as-google-pa.sandbox.googleapis.com"; //dev

  private static final String MIME_TYPE = "application/vnd.android.package-archive";
  private static final int CHUNK_SIZE = 1024;
  private static final int KB = 0x400;
  private static final String UPLOAD_SOURCE = "ANDROID_STUDIO";

  private static final String URL_TYPE_KEY = "@type";
  private static final String URL_TYPE =
    "type.googleapis.com/google.internal.fetchasgoogle.v1.FetchResponse";
  private static final String STATUS_KEY = "status";
  private static final String SCREENSHOT_DATA_KEY = "screenshotData";
  private static final String REFERENCED_RESOURCE_KEY = "referencedResource";
  private static final String REQUEST_KEY = "request";
  private static final String RESPONSE_KEY = "response";
  private static final String FETCH_STATUS_KEY = "fetchStatus";
  private static final String OK_STATUS = "OK";
  private static final String PENDING_STATUS = "PENDING";
  private static final int FETCH_TIME_INTERVAL_IN_MS = 5000;

  private FetchAsGooglePa myStub;

  public static FetchAsGoogleClient createInstance() throws FetchAsGoogleException {
    FetchAsGooglePa stub = createStub();
    if (stub == null) {
      throw new FetchAsGoogleException(
          "Could not find user credentials", FetchAsGoogleException.ErrorCode.NO_CREDENTIAL);
    }
    return new FetchAsGoogleClient(stub);
  }

  private FetchAsGoogleClient(FetchAsGooglePa stub) {
    myStub = stub;
  }

  @Nullable
  private static FetchAsGooglePa createStub() {
    CredentialedUser user = GoogleLogin.getInstance().getActiveUser();
    if (user != null) {
      return new FetchAsGooglePa.Builder(httpTransport, jsonFactory,
          new FetchAsGoogleHttpRequestInitializer(user.getCredential()))
              .setServicePath(SERVER_PATH).setRootUrl(ROOT_URL).build();
    }
    return null;
  }

  /**
   * Uploads the apk file to Fetch as Google backend server. This method executes a network request
   * synchronously and may take a while to finish.
   * @param packageId The package id of the apk.
   * @param apkFile The apk file to be uploaded.
   * @return The ApkHolder returned from backend server.
   */
  @NotNull
  public ApkHolder uploadApk(@NotNull String packageId, @NotNull File apkFile)
      throws FetchAsGoogleException, IOException {
    ApkDataRef apkDataRef = myStub.apks().startUpload().execute();

    FetchAsGooglePa.Media.Upload upload = myStub.media()
        .upload(apkDataRef.getResourceName(), null, new FileContent(MIME_TYPE, apkFile));
    upload.getMediaHttpUploader().setDirectUploadEnabled(true);
    upload.getMediaHttpUploader().setChunkSize(CHUNK_SIZE * KB);

    HttpResponse response = upload.executeUnparsed();
    if (response.getStatusCode() != 200) {
      throw new FetchAsGoogleException(
          "Failed to upload Apk: " + response, FetchAsGoogleException.ErrorCode.SERVER_ERROR);
    }

    Apk feagApk = new Apk().setPackageId(packageId).setSource(UPLOAD_SOURCE);
    ApkHolder apk = new ApkHolder().setApk(feagApk).setApkDataRef(apkDataRef);
    return myStub.apks().create(apk).execute();
  }

  /**
   * Fetches the app content as Google with given deep link.
   * @param deepLink The deep link which tells Google backend which app content to fetch.
   * @param apkHolder ApkHolder returned by FetchAsGoogleClient#uploadApk.
   */
  @NotNull
  public FetchResponse fetchAsGoogle(@NotNull String deepLink, @NotNull ApkHolder apkHolder)
      throws FetchAsGoogleException, IOException, InterruptedException {
    Operation startFetchResult = startFetch(deepLink, apkHolder);
    if (startFetchResult == null) {
      throw new FetchAsGoogleException(
          "Operation failed with an empty response", FetchAsGoogleException.ErrorCode.SERVER_ERROR);
    }

    Operation result = null;
    String fetchToken = startFetchResult.getName();
    if (fetchToken == null || fetchToken.isEmpty()) {
      throw new FetchAsGoogleException(
          "No fetch token returned", FetchAsGoogleException.ErrorCode.SERVER_ERROR);
    }

    do {
      Thread.sleep(FETCH_TIME_INTERVAL_IN_MS);
      result = getFetchResult(fetchToken);
      if (result == null) {
          throw new FetchAsGoogleException(
              "Operation failed with an empty response",
              FetchAsGoogleException.ErrorCode.SERVER_ERROR);
      }
      if (result.getDone() == null || !result.getDone()) {
        // Still pending of result, continue.
        continue;
      }

      if (result.getError() != null) {
        throw new FetchAsGoogleException(
            "Operation failed with error status: " + result.getError(),
            FetchAsGoogleException.ErrorCode.GENERIC_ERROR);
      }
      if (result.getResponse() == null) {
        throw new FetchAsGoogleException(
            "Operation failed with an empty response", FetchAsGoogleException.ErrorCode.SERVER_ERROR);
      }
      if (!result.getResponse().get(URL_TYPE_KEY).equals(URL_TYPE)) {
        throw new FetchAsGoogleException(
            "Operation response contains unexpected type: " + result.getResponse().get(URL_TYPE_KEY),
            FetchAsGoogleException.ErrorCode.SERVER_ERROR);
      }
      if (result.getResponse().get(STATUS_KEY) == null) {
        throw new FetchAsGoogleException(
            "Operation failed with an empty status", FetchAsGoogleException.ErrorCode.SERVER_ERROR);
      }
      if (!result.getResponse().get(STATUS_KEY).equals(OK_STATUS)) {
        throw new FetchAsGoogleException(
            "Operation failed with error status: " + result.getResponse().get(STATUS_KEY),
            FetchAsGoogleException.ErrorCode.GENERIC_ERROR);
      }
    } while (result.getDone() == null || !result.getDone());
    return constructFetchResponse(result.getResponse());
  }

  @NotNull
  private FetchResponse constructFetchResponse(Map<String,Object> map) {
    FetchResponse response = new FetchResponse();
    response.setStatus((String)map.get(STATUS_KEY));
    if (map.get(SCREENSHOT_DATA_KEY) != null) {
      response.setScreenshotData((String)map.get(SCREENSHOT_DATA_KEY));
    }
    List<Map<String, Object>> referencedResource =
      (List<Map<String, Object>>) map.get(REFERENCED_RESOURCE_KEY);
    if (referencedResource != null) {
      List<ReferencedResource> resources = Lists.newArrayList();
      for (Map<String, Object> resource : referencedResource) {
        resources.add(
          new ReferencedResource().setFetchStatus((String)resource.get(FETCH_STATUS_KEY))
            .setRequest((String)resource.get(REQUEST_KEY))
            .setResponse((String)resource.get(RESPONSE_KEY)));
      }
    }
    return response;
  }

  private Operation startFetch(String deepLink, ApkHolder apkHolder) throws IOException {
    FetchRequest request = new FetchRequest()
      .setPackageId(apkHolder.getApk().getPackageId())
      .setApkId(apkHolder.getApk().getApkId())
      .setIntent(new Intent().setUri(deepLink))
      .setSource(UPLOAD_SOURCE);
    //TODO: Think about how to get language for request.

    FetchAsGooglePa.Apks.Fetch fetch = createStub().apks().fetch(request);
    return fetch.execute();
  }

  private Operation getFetchResult(String fetchToken) throws IOException {
    FetchAsGooglePa.Operations.Get get = createStub().operations().get(fetchToken);
    return get.execute();
  }

  public static class FetchAsGoogleException extends Exception {
    public enum ErrorCode {
      NO_CREDENTIAL,
      SERVER_ERROR, // Error due to server
      GENERIC_ERROR // Error due to client
    }
    @NotNull ErrorCode myErrorCode;

    public FetchAsGoogleException(String message, @NotNull ErrorCode code) {
      super(message);
      myErrorCode = code;
    }

    public ErrorCode getErrorCode() {
      return myErrorCode;
    }
  }

  private static class FetchAsGoogleHttpRequestInitializer implements HttpExecuteInterceptor, HttpRequestInitializer {
    private final Credential access;

    public FetchAsGoogleHttpRequestInitializer(Credential access) {
      this.access = access;
    }

    @Override
    public void initialize(HttpRequest request) throws IOException {
      access.initialize(request);
      request.setInterceptor(this);
    }

    @Override
    public void intercept(HttpRequest request) throws IOException {
      access.intercept(request);
    }
  }
}
