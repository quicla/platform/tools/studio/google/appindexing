/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appindexing.api;

import com.google.common.base.CharMatcher;
import com.google.common.collect.Lists;

import com.intellij.psi.JavaRecursiveElementVisitor;
import com.intellij.psi.PsiCodeBlock;
import com.intellij.psi.PsiDeclarationStatement;
import com.intellij.psi.PsiExpressionStatement;
import com.intellij.psi.PsiStatement;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * A filter for searching a part of code and getting statements which contains the filter string.
 * <p/>
 * Example:
 * Given filter string "start", StatementFilter returns
 * "AppIndex.AppIndexApi.start(mClient, viewAction);" for the code below.
 *      int a = 0;
 *      if (a == 0) {
 *        AppIndex.AppIndexApi.start(mClient, viewAction);
 *      }
 */
public final class StatementFilter extends JavaRecursiveElementVisitor {

  private String myFilterString;

  private List<PsiStatement> myStatements = Lists.newArrayList();

  public StatementFilter(@NotNull String filterString) {
    myFilterString = CharMatcher.WHITESPACE.removeFrom(filterString);
  }

  @NotNull
  public List<PsiStatement> getStatements() {
    return myStatements;
  }

  @Override
  public void visitExpressionStatement(PsiExpressionStatement statement) {
    if (CharMatcher.WHITESPACE.removeFrom(statement.getText()).contains(myFilterString)) {
      myStatements.add(statement);
    }
  }

  @Override
  public void visitDeclarationStatement(PsiDeclarationStatement statement) {
    if (CharMatcher.WHITESPACE.removeFrom(statement.getText()).contains(myFilterString)) {
      myStatements.add(statement);
    }
  }

  /**
   * Filters statements in a code block with the filter string.
   *
   * @param codeBlock The code block to process.
   * @return Statements that contain the string and pass the filter.
   */
  @NotNull
  public static List<PsiStatement> filterCodeBlock(@NotNull String filterString, @NotNull PsiCodeBlock codeBlock) {
    StatementFilter filter = new StatementFilter(filterString);
    codeBlock.accept(filter);
    return filter.getStatements();
  }

  /**
   * Filters statements in the list.
   *
   * @param statements The list of statements.
   * @return Statements that contain the string and pass the filter.
   */
  @NotNull
  public static List<PsiStatement> filterStatements(@NotNull String filterString, @NotNull List<PsiStatement> statements) {
    StatementFilter filter = new StatementFilter(filterString);
    for (PsiStatement statement : statements) {
      statement.accept(filter);
    }
    return filter.getStatements();
  }
}
